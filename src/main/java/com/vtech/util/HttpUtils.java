package com.vtech.util;

import org.hamcrest.core.IsInstanceOf;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.vtech.dto.CommDTO;
import com.vtech.dto.KeywordDTO;
import com.vtech.dto.ProductDTO;

public class HttpUtils {

	private static String urlFormat = "http://%s:%s/service/push%sToQueue";


	public static void postDTO(CommDTO dto) {
		RestTemplate rt = new RestTemplate();
		rt.getMessageConverters().add(new StringHttpMessageConverter());
		String uri = "";

		if (dto instanceof ProductDTO) {
			dto = (ProductDTO) dto;
			uri = String.format(urlFormat, dto.getClientIp(), dto.getClientPort(), dto.getClass());
			rt.postForObject(uri, dto, ProductDTO.class);
		}
			
		else if (dto instanceof KeywordDTO) {
			dto = (KeywordDTO) dto;
			String.format(urlFormat, dto.getClientIp(), dto.getClientPort(), dto.getClass());
			rt.postForObject(uri, dto, KeywordDTO.class);
		}
			
	}

}
