package com.vtech.util;

public class NetworkMonitorUtil {
	public static int successCount = 0;
	public static int failCount = 0;
	
	public static void printStatus() {
		System.out.println(String.format("Network success count: %d, fail count: %d", successCount, failCount));
	}
	
}
