/*
 * package com.vtech.dao;
 * 
 * import javax.transaction.Transactional;
 * 
 * import org.springframework.data.jpa.repository.JpaRepository; import
 * org.springframework.data.jpa.repository.Modifying; import
 * org.springframework.data.jpa.repository.Query;
 * 
 * import entity.KeywordStatus;
 * 
 * public interface KeywordStatusRepository extends JpaRepository<KeywordStatus,
 * Long>{
 * 
 * @Transactional
 * 
 * @Modifying
 * 
 * @Query(value = "truncate table keyword_status",nativeQuery = true) public
 * void truncateTable(); }
 */