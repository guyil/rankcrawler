/*
 * package com.vtech.dao;
 * 
 * import javax.transaction.Transactional;
 * 
 * import org.springframework.data.jpa.repository.JpaRepository; import
 * org.springframework.data.jpa.repository.Modifying; import
 * org.springframework.data.jpa.repository.Query; import
 * org.springframework.stereotype.Repository;
 * 
 * import entity.Product;
 * 
 * @Repository public interface ProductRepository extends JpaRepository<Product,
 * Long> {
 * 
 * @Transactional
 * 
 * @Modifying
 * 
 * @Query(value = "truncate table product",nativeQuery = true) public void
 * truncateTable(); }
 */