/*package com.vtech.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.vtech.entity.Brand;
import com.vtech.entity.Keyword;

public interface KeywordRepository extends JpaRepository<Keyword, Long>{

	//List<ProductListingRank> findDistinctBySku(String sku);
	
	List<Keyword> findBySku(String sku);

	List<Keyword> findByActive(boolean b);
	
	List<Keyword> findByBrand(Brand b);
	
	List<Keyword> findByCoreAndCountryCode(boolean b,String country);
	
	List<Keyword> findByCoreAndActive(boolean b, boolean active);

	Optional<Keyword> findById(Long id);
	
	Keyword findBySkuAndNameAndMatchTypeAndGroupName(String sku, String name, String matchType, String GroupName);
	
	@Query("select k from Keyword k where k.sku = ?1 and k.name = ?2 and k.matchType = ?3 and k.groupName is null")
	Keyword findBySkuAndNameAndMatchTypeAndEmptyGroupName(String sku, String name, String matchType);

	Keyword findBySkuAndNameAndMatchTypeAndGroupNameAndCountryCode(String sku, String keyword, String matchType,
			String groupName, String countryCode);

	List<Keyword> findByNameAndActive(String keyword, boolean active);
	
	@Modifying
	@Query("update Keyword set core=true where name = ?1 and sku=?2 and matchType = 'Exact'")
	void updateCoreByKeywordAndSku(String keyword, String sku);
	
}
*/