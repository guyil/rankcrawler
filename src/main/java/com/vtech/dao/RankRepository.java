/*
 * package com.vtech.dao;
 * 
 * import javax.transaction.Transactional;
 * 
 * import org.springframework.data.jpa.repository.JpaRepository; import
 * org.springframework.data.jpa.repository.Modifying; import
 * org.springframework.data.jpa.repository.Query;
 * 
 * import entity.Rank;
 * 
 * public interface RankRepository extends JpaRepository<Rank, Long>{
 * 
 * @Transactional
 * 
 * @Modifying
 * 
 * @Query(value = "truncate table rank",nativeQuery = true) public void
 * truncateTable(); }
 */