package com.vtech.factory;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import actions.CampaignAction;
import actions.UserAction;
import utils.DriverUtils;

public class DriverUtilFactory {

	
	
	
	private static DriverUtils buyerDriverUtilUK;
	private static DriverUtils buyerDriverUtilDE;
	
	private static DriverUtils buyerDriverUtil;

	static CampaignAction campaignAction;
	

	
	public static DriverUtils getBuyerDriverUtil(String countryCode) {
		
		if(countryCode.equals("UK") && buyerDriverUtilUK == null){
			buyerDriverUtilUK = new DriverUtils(true, false);
			buyerDriverUtilUK.setCountryCode("UK");
			return buyerDriverUtilUK;
		}else if(countryCode.equals("DE")&& buyerDriverUtilDE == null){
			buyerDriverUtilDE =new DriverUtils(true, false);
			buyerDriverUtilDE.setCountryCode("DE");
			return buyerDriverUtilDE;
		}else{
			buyerDriverUtil = new DriverUtils(true, false);
			return buyerDriverUtil;
		}
			
	}
	



}
