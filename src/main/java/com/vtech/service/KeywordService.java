package com.vtech.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.vtech.controller.CommController;
import com.vtech.dto.KeywordDTO;
import com.vtech.factory.DriverUtilFactory;

import actions.UserAction;
import entity.KeywordStatus;
import entity.ProductListing;
import testcases.CheckAdsSearchResult;
import utils.DriverUtils;

@Component
public class KeywordService {
	
	
	private static final Logger log = LoggerFactory.getLogger(KeywordService.class);

	public void fetchKeywordRanksByAsins(Map<String, String> skuAsinMap, List<KeywordStatus> keywordStatusList) {

		String countryCode = "US";

		DriverUtils driverUtil = new DriverUtils();
		UserAction userAction = new UserAction(driverUtil);
		userAction.navigateToHomePage();

		Map<String, ArrayList<KeywordStatus>> map = new HashMap<>();
		
		//keywordStatusList = keywordStatusList.subList(0, 3);

		for (KeywordStatus keywordStatus : keywordStatusList) {

			if (keywordStatus.getCountryCode() != null && !keywordStatus.getCountryCode().equals(countryCode)) {
				driverUtil.setCountryCode(countryCode);
				userAction = new UserAction(driverUtil);
				userAction.navigateToHomePage();
			}

			if (!map.containsKey(keywordStatus.getKeyword())) {
				map.put(keywordStatus.getKeyword(), new ArrayList<KeywordStatus>());
			}
			map.get(keywordStatus.getKeyword()).add(keywordStatus);

		}

		for (String keyword : map.keySet()) {
			fetchSelectedAdsRank(keyword, map.get(keyword), skuAsinMap, driverUtil);
		}
		
		driverUtil.tearDownBrowser();

	}

	public void fetchSelectedAdsRank(String keyword, List<KeywordStatus> keywordStatusList,
			Map<String, String> skuAsinMap, DriverUtils driverUtil) {
		try {
			CheckAdsSearchResult checkAdsSearchResult = new CheckAdsSearchResult(driverUtil);

			Set<String> asins = new HashSet<String>();

			Map<String, List<KeywordStatus>> map = new HashMap<>();

			if (keywordStatusList != null && keywordStatusList.size() == 0) {
				return;
			}

			for (KeywordStatus keywordStatus : keywordStatusList) {

				String asin = skuAsinMap.get(keywordStatus.getSku());
				asins.add(asin);

				if (!map.containsKey(asin))
					map.put(asin, new ArrayList<KeywordStatus>());

				map.get(asin).add(keywordStatus);
				keywordStatus.resetRank();

			}

			List<ProductListing> productListings = checkAdsSearchResult.getProductListingAdAndNaturalRank(keyword,
					new ArrayList<String>(asins), null);

			for (ProductListing productListing : productListings) {
				System.out.println(productListing);
				List<KeywordStatus> selectedKeywordStatusList = map.get(productListing.getAsin());

				for (KeywordStatus selectedKeywordStatus : selectedKeywordStatusList) {
					if (selectedKeywordStatus != null) {
						selectedKeywordStatus.setRank(productListing.getRank());
						selectedKeywordStatus.setPageNum(productListing.getPageNum());
						selectedKeywordStatus.setNumInPage(productListing.getNumInPage());
						selectedKeywordStatus.setAdsRank(productListing.getAdsRank());
						selectedKeywordStatus.setAdsPageNum(productListing.getAdsPageNum());
						selectedKeywordStatus.setAdsNumInPage(productListing.getAdsNumInPage());
					}
				}
			}

			return;
		} catch (Exception e) {
			log.error(e.getMessage());
			return;
		} finally {
		}

	}
	/*
	private List<KeywordStatus> fetchKeywordRanksByAsins(List<Keyword> keywordsList) {
		DriverUtils driverUtil = null;

		UserAction userAction = new UserAction(driverUtil);
		userAction.navigateToHomePage();

		Map<String, ArrayList<Keyword>> map = new HashMap<>();

		for (Keyword keyword : keywordsList) {

			if (!map.containsKey(keyword.getName())) {
				map.put(keyword.getName(), new ArrayList<Keyword>());
			}
			map.get(keyword.getName()).add(keyword);

		}

		List<KeywordStatus> keywordStatusList = new ArrayList<KeywordStatus>();

		for (String keyword : map.keySet()) {
			driverUtil = DriverUtilFactory.getBuyerDriverUtil(map.get(keyword).get(0).getCountryCode());
			keywordStatusList.addAll(fetchSelectedAdsRank(keyword, map.get(keyword), driverUtil));
		}

		return keywordStatusList;
	}

	public List<KeywordStatus> fetchSelectedAdsRank(String keyword, List<Keyword> keywordList, DriverUtils driverUtil) {
		try {
			CheckAdsSearchResult checkAdsSearchResult = new CheckAdsSearchResult(driverUtil);

			Set<String> asins = new HashSet<String>();

			Map<String, List<KeywordStatus>> map = new HashMap<>();

			if (keywordList != null && keywordList.size() == 0) {
				return null;
			}

			List<KeywordStatus> keywordStatusList = new ArrayList<KeywordStatus>();

			for (Keyword selectedKeyword : keywordList) {
				KeywordStatus keywordStatus = new KeywordStatus();
				keywordStatus.setAsin(selectedKeyword.getAsin());
				keywordStatus.setKeyword(selectedKeyword.getName());
				keywordStatus.setCountryCode(selectedKeyword.getCountryCode());

				keywordStatusList.add(keywordStatus);
			}

			for (KeywordStatus keywordStatus : keywordStatusList) {

				String asin = keywordStatus.getAsin();
				asins.add(asin);

				if (!map.containsKey(asin))
					map.put(asin, new ArrayList<KeywordStatus>());

				map.get(asin).add(keywordStatus);
				keywordStatus.setAdsPageNum(999);

			}

			List<ProductListing> productListings = checkAdsSearchResult.getProductListingAdAndNaturalRank(keyword,
					new ArrayList<String>(asins), null);

			for (ProductListing productListing : productListings) {
				List<KeywordStatus> selectedKeywordStatusList = map.get(productListing.getAsin());

				for (KeywordStatus selectedKeywordStatus : selectedKeywordStatusList) {
					if (selectedKeywordStatus != null) {
						selectedKeywordStatus.setRank(productListing.getRank());
						selectedKeywordStatus.setPageNum(productListing.getPageNum());
						selectedKeywordStatus.setNumInPage(productListing.getNumInPage());
						selectedKeywordStatus.setAdsRank(productListing.getAdsRank());
						selectedKeywordStatus.setAdsPageNum(productListing.getAdsPageNum());
						selectedKeywordStatus.setAdsNumInPage(productListing.getAdsNumInPage());
					}
				}
			}

			return keywordStatusList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
		}

	}

	public KeywordStatus fetchSelectedAdsRank(Keyword keyword, DriverUtils driverUtil) {
		KeywordStatus keywordStatus = new KeywordStatus();
		try {
			CheckAdsSearchResult checkAdsSearchResult = new CheckAdsSearchResult(driverUtil);

			ProductListing productListing = checkAdsSearchResult.getProductListingAdAndNaturalRank(keyword.getName(),
					keyword.getAsin(), keyword.getId());

			keywordStatus.setAsin(keyword.getAsin());
			keywordStatus.setCountryCode(keyword.getCountryCode());
			if (productListing != null) {
				keywordStatus.setRank(productListing.getRank());
				keywordStatus.setPageNum(productListing.getPageNum());
				keywordStatus.setNumInPage(productListing.getNumInPage());
				keywordStatus.setAdsRank(productListing.getAdsRank());
				keywordStatus.setAdsPageNum(productListing.getAdsPageNum());
				keywordStatus.setAdsNumInPage(productListing.getAdsNumInPage());
			} else {
				keywordStatus.setAdsPageNum(999);
			}
			return keywordStatus;
		} catch (Exception e) {
			e.printStackTrace();
			return keywordStatus;
		} finally {
		}

	}*/

	/*
	 * @Transactional public void saveKeyWordList(KeywordDTO dto) { if(dto!=null) {
	 * List<KeywordStatus> list = dto.getKeywordStatusList();
	 * if(list!=null&&list.size()>0) { keywordStatusRepository.truncateTable();
	 * keywordStatusRepository.saveAll(list); } } }
	 */
}
