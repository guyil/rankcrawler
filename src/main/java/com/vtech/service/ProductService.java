package com.vtech.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;
import com.vtech.dto.ProductDTO;
import entity.Product;
import entity.Rank;
import testcases.GetProductInformation;
import utils.DriverUtils;

@Component
public class ProductService {


	public void fetchProductInfo(ProductDTO productDTO) {

		List<Product> productList = productDTO.getProductList();

		List<Rank> rankList = new ArrayList<Rank>();

		DriverUtils driverUtil = new DriverUtils();

		List<Product> updatedProductList = new ArrayList<Product>();

		GetProductInformation getProductInformation = new GetProductInformation(driverUtil);

		// productList = productList.subList(0, 2);

		for (Product product : productList) {
			try {
				if (!product.getCountry().equals(driverUtil.getCountryCode())) {
					driverUtil.setCountryCode(product.getCountry());
					getProductInformation = new GetProductInformation(driverUtil);
				}

				Product updatedProduct = null;
				if (productDTO.isSelf()) {
					updatedProduct = getProductInformation.getSelfProductInformation(product.getAsin());
				} else {
					updatedProduct = getProductInformation.getProductInformation(product.getAsin());
				}

				if (updatedProduct == null) {
					continue;
				} else {
					copyProductFields(product, updatedProduct);
					updatedProductList.add(updatedProduct);
				}

				List<Rank> ranks = getProductInformation.getProductRank(product.getAsin());

				for (Rank rank : ranks) {
					rank.setCountryCode(product.getCountry());

					rank.setPrice(product.getPrice());
					rank.setCountryCode(product.getCountry());
					if (rank.isGeneral()) {
						product.setGeneralRank(rank.getRank());
						product.setGeneralCategory(rank.getCategory());
						rank.setReviewCount(product.getReviewCount());
						// break;
					}

					rank.setProductId(product.getId());
				}

				rankList.addAll(ranks);

			} catch (Exception e) {
				e.printStackTrace();
				driverUtil.getDriver().navigate().refresh();
			}
		}

		productDTO.setProductList(updatedProductList);
		productDTO.setRankList(rankList);

		driverUtil.tearDownBrowser();

	}

	private void copyProductFields(Product product, Product updatedProduct) {
		updatedProduct.setTotalOrders(product.getTotalOrders());
		updatedProduct.setId(product.getId());
		updatedProduct.setTag(product.getTag());
		updatedProduct.setCountry(product.getCountry());
		updatedProduct.setSize(product.getSize());
		updatedProduct.setWeight(product.getWeight());
		updatedProduct.setNote(product.getNote());
		updatedProduct.setCheckedReview(product.isCheckedReview());
		updatedProduct.setSelf(product.isSelf());
		updatedProduct.setSkuTag(product.getSkuTag());
		updatedProduct.setBaseTotalOrders(product.getBaseTotalOrders());
		updatedProduct.setCreatedDate(product.getCreatedDate());
	}

	/*
	 * @Transactional public void saveProductDTO(ProductDTO productDTO) {
	 * 
	 * List<Product> products = productDTO.getProductList();
	 * 
	 * List<Rank> rankList = productDTO.getRankList();
	 * 
	 * 
	 * if(products!=null&&products.size()>0) { productRepository.truncateTable(); }
	 * 
	 * for (Product product : products) { // if (product.isSelf()) //
	 * checkFollowBuyBox(product); try { productRepository.saveAndFlush(product); }
	 * catch (Exception e) { System.out.println(product); e.printStackTrace(); } }
	 * 
	 * if(rankList!=null&&rankList.size()>0) { rankRepository.truncateTable();
	 * rankRepository.saveAll(rankList); }
	 * 
	 * 
	 * }
	 */
}
