package com.vtech.service;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class FutureTest {

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		CompletableFuture<String> completableFuture
		  = CompletableFuture.supplyAsync(() -> "Hello");
		 
		CompletableFuture<Void> future = completableFuture
		  .thenAccept(s -> System.out.println("Computation returned: " + s));
		 
		future.get();
	}
	
	public static Future<String> calculateAsync() throws InterruptedException {
	    CompletableFuture<String> completableFuture 
	      = new CompletableFuture<>();
	 
	    Executors.newCachedThreadPool().submit(() -> {
	        Thread.sleep(5000);
	        completableFuture.complete("Hello");
	        return null;
	    });
	 
	    return completableFuture;
	}

}
