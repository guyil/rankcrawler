package com.vtech.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vtech.dto.CommDTO;
import com.vtech.dto.InterruptionRecoveryDTO;
import com.vtech.dto.KeywordDTO;
import com.vtech.dto.ProductDTO;
import com.vtech.service.KeywordService;
import com.vtech.service.ProductService;
import com.vtech.util.HttpUtils;
import com.vtech.util.NetworkMonitorUtil;

import entity.KeywordStatus;

@RestController
public class CommController {

	private static final Logger log = LoggerFactory.getLogger(CommController.class);

	@Autowired
	KeywordService keywordService;

	@Autowired
	ProductService productService;

	@Autowired
	InterruptionRecoveryDTO interruptionRecoveryDTO;

	@RequestMapping(value = "/service/fetchRank", method = RequestMethod.POST)
	@ResponseBody
	public void fetchKeywordRank(@RequestBody KeywordDTO keywordDTO) {

		log.info("Start:" + keywordDTO.toString());

		keywordService.fetchKeywordRanksByAsins(keywordDTO.getSkuAsinMap(), keywordDTO.getKeywordStatusList());

		try {
			HttpUtils.postDTO(keywordDTO);
			log.info("Finish" + keywordDTO.toString());
			NetworkMonitorUtil.successCount++;
		} catch (Exception e) {
			interruptionRecoveryDTO.addToRecoveryDTOList(keywordDTO);
			log.error(new Date() + "   resend:" + keywordDTO.toString() + "; Method:fetchProductInfo");
			log.info("Failed:" + keywordDTO.toString());
			NetworkMonitorUtil.failCount++;
		}

		NetworkMonitorUtil.printStatus();

	}

	@RequestMapping(value = "/service/fetchProductInfo", method = RequestMethod.POST)
	@ResponseBody
	public void fetchProductInfo(@RequestBody ProductDTO productDTO) throws InterruptedException {

		ProductDTO currentProductDTO = (ProductDTO) productDTO;

		log.info("Start" + productDTO.toString());

		productService.fetchProductInfo(currentProductDTO);

		try {
			HttpUtils.postDTO(productDTO);
			log.info("Finish" + productDTO.toString());
			NetworkMonitorUtil.successCount++;
		} catch (Exception e) {
			interruptionRecoveryDTO.addToRecoveryDTOList(productDTO);
			log.error(new Date() + "   Add to REcov:" + productDTO.toString() + "; Method:fetchProductInfo");
			log.info("Failed:" + productDTO.toString());
			NetworkMonitorUtil.failCount++;
		}

		NetworkMonitorUtil.printStatus();

	}

	@RequestMapping(value = "/check", method = RequestMethod.GET)
	public String check() {

		return "success";

	}

	@RequestMapping(value = "/service/checkPost", method = RequestMethod.POST)
	public String checkPost() {

		return "success";

	}

	@RequestMapping(value = "/test", method = RequestMethod.GET)
	@ResponseBody
	public void test() {

		KeywordDTO keywordStatusDTO = new KeywordDTO(new Long(1));
		keywordStatusDTO.setHostIp("localhost");
		keywordStatusDTO.setHostPort("8080");
		keywordStatusDTO.setExecuteMethodName("fetchRank");
		keywordStatusDTO.setCallbackMethodName("fetchRank");

		Map<String, String> skuAsinMap = new HashMap<>();
		skuAsinMap.put("AS1080143", "B06ZZ19MS3");

		List<KeywordStatus> keywordStatusList = new ArrayList<>();

		keywordStatusList.add(new KeywordStatus("AS1080143", "auvon tens"));

		keywordStatusDTO.setKeywordStatusList(keywordStatusList);
		keywordStatusDTO.setSkuAsinMap(skuAsinMap);

		// keywordService.fetchKeywordRanksByAsins(keywordStatusDTO.getSkuAsinMap(),
		// keywordStatusDTO.getKeywordStatusList());

		HttpUtils.postDTO(keywordStatusDTO);

	}

	@RequestMapping(value = "/testDTO", method = RequestMethod.GET)
	@ResponseBody
	public void testDTO() {

		ProductDTO currentProductDTO = new ProductDTO(new Long(new Date().getTime()));

		interruptionRecoveryDTO.addToRecoveryDTOList(currentProductDTO);

	}

}
