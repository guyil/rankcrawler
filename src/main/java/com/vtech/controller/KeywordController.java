package com.vtech.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.vtech.service.KeywordService;

import entity.KeywordStatus;

@RestController
public class KeywordController {
	
	@Autowired
	KeywordService keywordService;
	
	
	
	public void fetchKeywordRanksByAsins(String countryCode, Map<String, String> skuAsinMap,
			List<KeywordStatus> keywordStatusList) {
		keywordService.fetchKeywordRanksByAsins(skuAsinMap, keywordStatusList);

	}
}
