package com.vtech;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
//@EntityScan(basePackages = { "com.vtech.entity"})
@EntityScan(basePackages = { "com.vtech.entity", "entity",  "com.amazonservices.mws.orders.entity"})
@EnableScheduling
public class RankCrawlerApplication {

	public static void main(String[] args) {
		SpringApplication.run(RankCrawlerApplication.class, args);
	}
	
//	@Bean(destroyMethod = "shutdown")
//    public Executor taskScheduler() {
//        return Executors.newScheduledThreadPool(5);
//    }

}
