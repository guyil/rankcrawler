/*
 * package com.vtech.entity;
 * 
 * import java.util.Date;
 * 
 * import javax.persistence.MappedSuperclass; import
 * javax.persistence.PrePersist; import javax.persistence.PreUpdate;
 * 
 * @MappedSuperclass public class BaseEntity { protected Date createdDate;
 * protected Date updatedDate;
 * 
 * public Date getUpdatedDate() { return updatedDate; }
 * 
 * public void setUpdatedDate(Date updatedDate) { this.updatedDate =
 * updatedDate; }
 * 
 * public Date getCreatedDate() { return createdDate; }
 * 
 * public void setCreatedDate(Date createdDate) { this.createdDate =
 * createdDate; }
 * 
 * @PreUpdate public void preUpdate() { this.updatedDate = new Date(); }
 * 
 * @PrePersist public void prePersist() { this.createdDate = new Date(); }
 * 
 * }
 */