/*
 * package com.vtech.entity;
 * 
 * import java.util.Date;
 * 
 * import javax.persistence.Column; import javax.persistence.Entity; import
 * javax.persistence.FetchType; import javax.persistence.GeneratedValue; import
 * javax.persistence.Id; import javax.persistence.JoinColumn; import
 * javax.persistence.ManyToOne; import javax.persistence.Table;
 * 
 * import org.hibernate.annotations.GenericGenerator;
 * 
 * 
 * 
 * 
 *//**
	 * Created by Yilin Gu on 5/14/2017.
	 *//*
		 * 
		 * @Entity
		 * 
		 * @Table(name = "keyword") public class Keyword extends BaseEntity {
		 * 
		 * @Id
		 * 
		 * @GenericGenerator(name = "generator", strategy = "increment")
		 * 
		 * @GeneratedValue(generator = "generator")
		 * 
		 * @Column(name = "keyword_id", unique = true, nullable = false) public Long id;
		 * 
		 * @Column(name = "asin") private String asin;
		 * 
		 * 
		 * @Column(name = "name") private String name;
		 * 
		 * @Column(name = "match_type") private String matchType;
		 * 
		 * private String countryCode;
		 * 
		 * 
		 * @Column(name = "is_active") private boolean active;
		 * 
		 * private boolean core;
		 * 
		 * private boolean isAuto;
		 * 
		 * private String groupName;
		 * 
		 * private float bid = 0.0f;
		 * 
		 * @ManyToOne(fetch = FetchType.EAGER)
		 * 
		 * @JoinColumn(name = "keyword_type_id") private KeywordType keywordType;
		 * 
		 * @ManyToOne(fetch = FetchType.EAGER)
		 * 
		 * @JoinColumn(name = "brand_id") private Brand brand;
		 * 
		 * // @Column(name = "created_date")
		 * 
		 * 
		 * 
		 * 
		 * 
		 * public Keyword() { }
		 * 
		 * public Keyword(String name, String asin) { this.name = name; active = false;
		 * createdDate = new Date(); }
		 * 
		 * public Keyword(String sku, String name, String matchType, String groupName,
		 * String countryCode) { if(sku !=null) sku = sku.toUpperCase(); this.asin =
		 * asin; this.name = name; this.matchType = matchType; this.groupName =
		 * groupName; this.countryCode = countryCode; this.active = true; createdDate =
		 * new Date(); }
		 * 
		 * public Long getId() { return id; }
		 * 
		 * public void setId(Long id) { this.id = id; }
		 * 
		 * 
		 * public String getName() { return name; }
		 * 
		 * public void setName(String name) { this.name = name; }
		 * 
		 * public String getMatchType() { return matchType; }
		 * 
		 * public void setMatchType(String matchType) { this.matchType = matchType; }
		 * 
		 * public boolean isActive() { return active; }
		 * 
		 * public void setActive(boolean active) { this.active = active; }
		 * 
		 * public boolean isAuto() { return isAuto; }
		 * 
		 * public void setAuto(boolean isAuto) { this.isAuto = isAuto; }
		 * 
		 * 
		 * 
		 * public String getGroupName() { return groupName; }
		 * 
		 * public void setGroupName(String groupName) { this.groupName = groupName; }
		 * 
		 * public float getBid() { return bid; }
		 * 
		 * public void setBid(float bid) { this.bid = bid; }
		 * 
		 * public String getCountryCode() { return countryCode; }
		 * 
		 * public void setCountryCode(String countryCode) { this.countryCode =
		 * countryCode; }
		 * 
		 * public Date getCreatedDate() { return createdDate; }
		 * 
		 * public void setCreatedDate(Date createdDate) { this.createdDate =
		 * createdDate; }
		 * 
		 * 
		 * 
		 * public KeywordType getKeywordType() { return keywordType; }
		 * 
		 * public void setKeywordType(KeywordType keywordType) { this.keywordType =
		 * keywordType; }
		 * 
		 * 
		 * 
		 * public String getAsin() { return asin; }
		 * 
		 * public void setAsin(String asin) { this.asin = asin; }
		 * 
		 * @Override public String toString() { return asin + ", " + name+ "," +
		 * groupName + "," + matchType + "," + countryCode + "," + isAuto +"," +
		 * createdDate; }
		 * 
		 * 
		 * @Override public int hashCode() { final int prime = 31; int result = 1;
		 * result = prime * result + ((id == null) ? 0 : id.hashCode()); result = prime
		 * * result + ((name == null) ? 0 : name.hashCode()); result = prime * result +
		 * ((asin == null) ? 0 : asin.hashCode()); return result; }
		 * 
		 * @Override public boolean equals(Object obj) { if (this == obj) return true;
		 * if (obj == null) return false; if (getClass() != obj.getClass()) return
		 * false; Keyword other = (Keyword) obj; if (id == null) { if (other.id != null)
		 * return false; } else if (!id.equals(other.id)) return false; if (name ==
		 * null) { if (other.name != null) return false; } else if
		 * (!name.equals(other.name)) return false; if (asin == null) { if (other.asin
		 * != null) return false; } else if (!asin.equals(other.asin)) return false;
		 * return true; }
		 * 
		 * public boolean isCore() { return core; }
		 * 
		 * public void setCore(boolean core) { this.core = core; }
		 * 
		 * public Brand getBrand() { return brand; }
		 * 
		 * public void setBrand(Brand brand) { this.brand = brand; }
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * }
		 */