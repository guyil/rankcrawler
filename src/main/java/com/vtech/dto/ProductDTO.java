package com.vtech.dto;

import java.util.List;

import entity.Product;
import entity.Rank;

public class ProductDTO extends CommDTO{

	/**
	 * 
	 */
	private static final long serialVersionUID = -919897527214058919L;
	
	public ProductDTO() {
	}
	

	public ProductDTO(Long actionId) {
		super(actionId);
	}

	List<Product> productList;
	
	List<Rank> rankList;
	
	boolean isSelf;

	public List<Product> getProductList() {
		return productList;
	}

	public void setProductList(List<Product> productList) {
		this.productList = productList;
	}

	public List<Rank> getRankList() {
		return rankList;
	}

	public void setRankList(List<Rank> rankList) {
		this.rankList = rankList;
	}

	public boolean isSelf() {
		return isSelf;
	}

	public void setSelf(boolean isSelf) {
		this.isSelf = isSelf;
	}

	
	
	
}
