package com.vtech.dto;

import java.util.List;
import java.util.Map;

import entity.KeywordStatus;

public class KeywordDTO extends CommDTO{
	
	private static final long serialVersionUID =-919897527214058919L;

	
	public KeywordDTO() {
	}
	
	public KeywordDTO(Long actionId) {
		super(actionId);
	}

	List<KeywordStatus> keywordStatusList;
	
	Map<String, String> skuAsinMap;

	public List<KeywordStatus> getKeywordStatusList() {
		return keywordStatusList;
	}

	public void setKeywordStatusList(List<KeywordStatus> keywordStatusList) {
		this.keywordStatusList = keywordStatusList;
	}

	public Map<String, String> getSkuAsinMap() {
		return skuAsinMap;
	}

	public void setSkuAsinMap(Map<String, String> skuAsinMap) {
		this.skuAsinMap = skuAsinMap;
	}
	
	
	
	
	
}
