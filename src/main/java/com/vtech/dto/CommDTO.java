package com.vtech.dto;

import java.io.Serializable;
import java.util.Date;

public abstract class CommDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	String callbackMethodName;
	String executeMethodName;
	String hostIp;
	String hostPort;

	String clientIp;
	String clientPort;

	Long actionId;
	String appName;
	String queueName;
	

	Date createdDate = new Date();
	
	public CommDTO() {
		
	}

	public CommDTO(Long actionId) {
		this.actionId = actionId;
	}

	public Long getActionId() {
		return actionId;
	}

	public String getCallbackMethodName() {
		return callbackMethodName;
	}

	public void setCallbackMethodName(String callbackMethodName) {
		this.callbackMethodName = callbackMethodName;
	}

	public String getExecuteMethodName() {
		return executeMethodName;
	}

	public void setExecuteMethodName(String executeMethodName) {
		this.executeMethodName = executeMethodName;
	}

	public String getHostIp() {
		return hostIp;
	}

	public void setHostIp(String hostIp) {
		this.hostIp = hostIp;
	}

	public String getHostPort() {
		return hostPort;
	}

	public void setHostPort(String hostPort) {
		this.hostPort = hostPort;
	}

	public String getClientIp() {
		return clientIp;
	}

	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}

	public String getClientPort() {
		return clientPort;
	}

	public void setClientPort(String clientPort) {
		this.clientPort = clientPort;
	}
	
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}


	
	
	public String getQueueName() {
		return queueName;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}


	@Override
	public String toString() {
		return "CommDTO [callbackMethodName=" + callbackMethodName + ", executeMethodName=" + executeMethodName
				+ ", hostIp=" + hostIp + ", hostPort=" + hostPort + ", clientIp=" + clientIp + ", clientPort="
				+ clientPort + ", actionId=" + actionId + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((actionId == null) ? 0 : actionId.hashCode());
		result = prime * result + ((callbackMethodName == null) ? 0 : callbackMethodName.hashCode());
		result = prime * result + ((clientIp == null) ? 0 : clientIp.hashCode());
		result = prime * result + ((clientPort == null) ? 0 : clientPort.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CommDTO other = (CommDTO) obj;
		if (actionId == null) {
			if (other.actionId != null)
				return false;
		} else if (!actionId.equals(other.actionId))
			return false;
		if (callbackMethodName == null) {
			if (other.callbackMethodName != null)
				return false;
		} else if (!callbackMethodName.equals(other.callbackMethodName))
			return false;
		if (clientIp == null) {
			if (other.clientIp != null)
				return false;
		} else if (!clientIp.equals(other.clientIp))
			return false;
		if (clientPort == null) {
			if (other.clientPort != null)
				return false;
		} else if (!clientPort.equals(other.clientPort))
			return false;
		return true;
	}
	
	
	

}
