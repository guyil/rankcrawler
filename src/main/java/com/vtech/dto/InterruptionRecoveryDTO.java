package com.vtech.dto;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.vtech.util.HttpUtils;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class InterruptionRecoveryDTO {

	private static final Logger log = LoggerFactory.getLogger(InterruptionRecoveryDTO.class);

	private List<CommDTO> dtoList = new ArrayList<>();
	private List<CommDTO> dtoListFromFile = new ArrayList<>();
	


	public void addToRecoveryDTOList(CommDTO dto) {
		dtoList.add(dto);
		/*
		 * try { List<CommDTO> existingDtoListFromFile = getDTOListFromFile();
		 * if(existingDtoListFromFile == null) existingDtoListFromFile= new
		 * ArrayList<>(); existingDtoListFromFile.add(dto);
		 * 
		 * } catch (ClassNotFoundException | IOException e) { log.debug(e.getMessage());
		 * }
		 */
	}

	@Scheduled(fixedDelay = 10000)
	public void resendPostRequestFromRecoveryList() throws Exception {

		dtoListFromFile = getDTOListFromFile();

		if (dtoList.isEmpty() && (dtoListFromFile == null || dtoListFromFile.isEmpty()))
			return;

		dtoList.addAll(dtoListFromFile);
		
		List<CommDTO> currentDtoList = new ArrayList<CommDTO>(dtoList);
		dtoList = new ArrayList<>();

		List<CommDTO> sentList = new ArrayList<>();
		List<CommDTO> expiredList = new ArrayList<>();

		Date expirationDate = getExpirationDate();

		for (CommDTO dto : currentDtoList) {
			try {
				HttpUtils.postDTO(dto);
			} catch (Exception e) {
				log.debug(e.getMessage());
				continue;
			}
			sentList.add(dto);

			if (dto.getCreatedDate() != null && dto.getCreatedDate().before(expirationDate))
				expiredList.add(dto);

		}

		for (CommDTO dto : sentList) {

			currentDtoList.remove(dto);
		}

		for (CommDTO dto : expiredList) {

			currentDtoList.remove(dto);
		}

		currentDtoList.addAll(dtoList);
		
		saveDTOListToFile(currentDtoList);
		
		
	}

	@PreDestroy
	public void destroy() {
		try {
			List<CommDTO> existingDtoListFromFile = getDTOListFromFile();
			if (existingDtoListFromFile == null)
				existingDtoListFromFile = new ArrayList<>();

			existingDtoListFromFile.addAll(dtoList);

			saveDTOListToFile(existingDtoListFromFile);

		} catch (IOException e) {
			log.debug(e.getMessage());
		}
	}

	private void saveDTOListToFile(List<CommDTO> dtoList2) throws IOException {
		
		if(dtoList2 == null)
			return;
		
		removeDuplicatesFromList(dtoList2); 
		
		FileOutputStream fos = new FileOutputStream("recoveryDto.tmp");
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(dtoList2);
		oos.close();

	}

	private void removeDuplicatesFromList(List<CommDTO> dtoList2) {
		Set<CommDTO> set = new LinkedHashSet<>(); 
        set.addAll(dtoList2); 
        dtoList2.clear(); 
        dtoList2.addAll(set);
	}

	private List<CommDTO> getDTOListFromFile() {
		FileInputStream fis;
		try {
			fis = new FileInputStream("recoveryDto.tmp");

			ObjectInputStream ois = new ObjectInputStream(fis);
			List<CommDTO> dtoList = (List<CommDTO>) ois.readObject();
			ois.close();
			return dtoList;
		} catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return dtoList;
		}
		
	}

	private Date getExpirationDate() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR, -6);
		Date expirationDate = cal.getTime();
		return expirationDate;
	}


}
